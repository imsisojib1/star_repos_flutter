import 'package:flutter_boilerplate_code/src/config/config_api.dart';
import 'package:flutter_boilerplate_code/src/core/application/api_interceptor.dart';
import 'package:flutter_boilerplate_code/src/core/application/token_service.dart';
import 'package:flutter_boilerplate_code/src/core/domain/interfaces/interface_api_interceptor.dart';
import 'package:flutter_boilerplate_code/src/features/home/applications/usecases/usecase_fetch_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/applications/usecases/usecase_fetch_repos_from_remote.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/datasources/datasource_cace_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/datasources/datasource_remote_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/enums/e_sort.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/repositories/repository_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_cache_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_remote_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/irepositories/interface_repository_repos.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  late UseCaseFetchReposFromRemote useCaseFetchRepos;
  IDataSourceRemoteRepos dataSourceRemoteRepos;
  IApiInterceptor apiInterceptor;
  TokenService tokenService;

  setUp(() async {
    tokenService = TokenService();
    apiInterceptor = ApiInterceptor(
      baseUrl: ConfigApi.baseUrl,
    );
    dataSourceRemoteRepos =
        DataSourceRemoteRepos(apiInterceptor: apiInterceptor, tokenService: tokenService);
    useCaseFetchRepos = UseCaseFetchReposFromRemote(remoteDataSource: dataSourceRemoteRepos,);
  });

  group('Request 10 Repositories', () {
    test('should fetch 10 repositories', () async {
      //arrange
      RequestParamsRepos requestParamsRepos = RequestParamsRepos(
        query: "Flutter",
        perPage: 10,
        page: 1,
        sort: ESort.stars.name,
      );

      //act
      var result = await useCaseFetchRepos.execute(requestParamsRepos);
      //assert
      expect(result.isRight(), true);
    });
  });
}
