## Star Repos Flutter
The project is all about to show repositories from Github using keyword "Flutter" with sorting options. Also offline functionality should work seamlessly using Flutter cross platform.

## Feature Showcasing Video
For understanding the app's flow you can try this [App Flow Video](https://drive.google.com/file/d/1PVLt6hlr2UTWTqFXttzWCZti1Ah8c4Ab/view?usp=sharing) . Thanks!

## Getting Started
To start the project I have used my own [boilerplate code](https://github.com/imsisojib/flutter_boilerplate_code) that helps me to skip re-code basic functionality of an app.

## Concept of UI
As there was no reference UI to build this app, I have inspired from Github mobile view as it was real world practice for me as well.

## Project Built With
```$xslt
  google_fonts: 6.1.0
  dartz: ^0.10.1
  provider: ^6.0.2
  flutter_screenutil: ^5.9.0
  fluro: ^2.0.3
  get_it: ^7.2.0
  shared_preferences: ^2.0.13
  http: ^1.2.0
  connectivity_plus: ^6.0.1
  permission_handler: ^10.0.1
  logger: ^1.1.0
  cached_network_image: ^3.3.1
  shimmer: ^3.0.0
```

## Tools and Techniques
- for Adaptive UI: flutter_screenutil
- for App Routing: fluro
- for Fonts: google_fonts
- for Cache/Local Database: shared_preferences
- for Dependency Injection: get_it
- for State Management: Provider Pattern
- for Functional Programming: dartz


## Project Architecture
I have used DDD (Domain Driven Development) pattern for this project to maintain SOLID principle. It helps me to organize my codebase by segmenting Feature.
Also used Repository pattern that makes abstraction properly. Defining abstraction class first then implementing it makes codebase more clean.
Also followed Functional programming to scope with Unit testing as making UseCases more independent and reusable.
For local database or caching I have used SharedPreference due to not being any interaction with the Remote Database as just showing data in offline.
To notify network connectivity as user could use the app both Online and Offline mode, I have followed subscription to the events of connectivity.

## Feature Solutions
- Fetched repositories using keyword "Flutter" with params sorting by "stars" and "updated"
- Stored fetched repositories into local database. Here in shared-preferences.
- From next if data available in cache, fetched from cached
- Every 30 minutes at the time of first page fetching called Remote database and cleared cached
- Repeat the task from start

### How the app looks like
**1.** Repo List Screen Light

<img src="screenshots/repo_list_light.png" alt="drawing" width="400"/>

**2.** Repo List Screen Dark

<img src="screenshots/repo_list_dark.png" alt="drawing" width="400"/>

**3.** Repo Details Screen Light

<img src="screenshots/repo_details_light.png" alt="drawing" width="400"/>

**4.** Repo Details Screen Dark

<img src="screenshots/repo_details_dark.png" alt="drawing" width="400"/>

**5.** Repo Details 404

<img src="screenshots/details_not_found_dark.png" alt="drawing" width="400"/>


## Want to try?
Here is the APK [Download](https://drive.google.com/file/d/1vmR4h5Sb5LIIOTU62fdHpzAkEVH24sFe/view?usp=sharing) link for Android.

## Todo In Future
- More Unit Testing
- UI testing for overall flow
- App's Flavor for Dev, QA, PROD environment
- Multiple Languages
- Feature for cleaning in-app cache