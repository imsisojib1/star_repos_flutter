import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/core/application/navigation_service.dart';
import 'package:flutter_boilerplate_code/src/features/connectivity/data/enums/e_connection_state.dart';
import 'package:flutter_boilerplate_code/src/features/connectivity/presentation/providers/provider_connectivity.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/providers/provider_repos.dart';
import 'package:flutter_boilerplate_code/src/resources/app_themes.dart';
import 'package:flutter_boilerplate_code/src/routes/router_helper.dart';
import 'package:flutter_boilerplate_code/src/routes/routes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import 'di_container.dart';

class MyApp extends StatefulWidget{
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final NavigationService navigationService = sl();
  StreamSubscription<List<ConnectivityResult>>? subscription;


  @override
  void initState() {
    super.initState();
    RouterHelper().setupRouter();
    subscription = Connectivity().onConnectivityChanged.listen((List<ConnectivityResult> result) {
      if (result.contains(ConnectivityResult.mobile) || result.contains(ConnectivityResult.wifi)) {
        context.read<ProviderConnectivity>().connectionState = EConnectionState.online;
        //switching back to online and checking if require to fetch data or not.
        if(context.read<ProviderRepos>().repoServeList.isEmpty){
          context.read<ProviderRepos>().fetchFlutterStarRepositories();
        }
      }else{
        context.read<ProviderConnectivity>().connectionState = EConnectionState.offline;
      }
    });
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: (_, app) {
        return MaterialApp(
          navigatorKey: navigationService.navigatorKey,
          debugShowCheckedModeBanner: false,
          builder: (context, child) {
            return ScrollConfiguration(
              //Removes the whole app's scroll glow
              behavior: AppBehavior(),
              child: child!,
            );
          },
          title: 'Star Repos',
          themeMode: ThemeMode.system,
          theme: buildLightTheme(context),
          darkTheme: buildDarkTheme(context),
          initialRoute: Routes.homeScreen,
          onGenerateRoute: RouterHelper.router.generator,
        );
      },
    );
  }
}

//to avoid scroll glow in whole app
class AppBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context,
      Widget child,
      AxisDirection axisDirection,
      ) {
    return child;
  }
}