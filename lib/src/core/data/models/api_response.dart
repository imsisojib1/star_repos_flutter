class ApiResponse<T>{
  int statusCode;
  T? result;
  String? message;

  ApiResponse({required this.statusCode, this.result, this.message,});

}