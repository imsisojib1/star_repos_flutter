class ConfigApi{
  static const String baseUrl = "https://api.github.com";

  //endpoints
  static const String repositories = "/repos";  //concat -> username/reponame
  static const String searchRepositories = "/search/repositories";  //concat-> query params
}