import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/features/errors/presentation/screens/screen_error.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/screens/screen_home.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/screens/screen_repository_details.dart';
import 'package:flutter_boilerplate_code/src/routes/routes.dart';


class RouterHelper {
  static final FluroRouter router = FluroRouter();

  static final Handler _homeScreenHandler =
  Handler(handlerFunc: (context, Map<String, dynamic> parameters) {
    return const ScreenHome();
  });
  static final Handler _repositoryDetailsScreenHandler =
  Handler(handlerFunc: (context, Map<String, dynamic> parameters) {
    return ScreenRepositoryDetails(
      arguments: ModalRoute.of(context!)!.settings.arguments as Map<String, dynamic>?,
    );
  });

  static final Handler _notFoundHandler =
  Handler(handlerFunc: (context, parameters) => const ScreenError());

  void setupRouter() {
    router.notFoundHandler = _notFoundHandler;

    router.define(Routes.homeScreen, handler: _homeScreenHandler, transitionType: TransitionType.cupertino);
    router.define(Routes.repositoryDetailsScreen, handler: _repositoryDetailsScreenHandler, transitionType: TransitionType.cupertino);
  }

}