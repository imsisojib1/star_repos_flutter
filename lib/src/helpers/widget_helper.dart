import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/di_container.dart';
import 'package:flutter_boilerplate_code/src/core/application/navigation_service.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class WidgetHelper{
  static String decorateNumberCount(int? count){
    if(count==null) return "0";
    if(count<1000) return "$count";
    return "${(count/1000).toStringAsFixed(1)}k";

  }

  static String decorateDateTimeAsPostingTime(String? updatedAt){
    if(updatedAt==null) return "";
    try{
      var updatedDateTime = DateTime.parse(updatedAt);
      Duration difference = DateTime.now().difference(updatedDateTime);

      if (difference.inDays > 0) {
        if(difference.inDays>1){
          return 'Updated ${difference.inDays} days ago';
        }
        return 'Updated ${difference.inDays} day ago';
      } else if (difference.inHours > 0) {
        if(difference.inHours>1){
          return 'Updated ${difference.inDays} hours ago';
        }
        return 'Updated ${difference.inHours} hour ago';
      } else if (difference.inMinutes > 0) {
        if(difference.inMinutes>1){
          return 'Updated ${difference.inMinutes} minutes ago';
        }
        return 'Updated ${difference.inMinutes} minute ago';
      } else {
        return 'Updated Just now';
      }

    }catch(e){
      return "";
    }

  }

  static String decorateUpdateTimeExactFormat(String? updatedAt){
    if(updatedAt==null) return "";
    try{
      var updatedDateTime = DateTime.parse(updatedAt);
      String month = "${updatedDateTime.month}".padLeft(2,"0");
      String day = "${updatedDateTime.day}".padLeft(2,"0");
      String year = "${updatedDateTime.year}";
      String hour = "${updatedDateTime.hour}".padLeft(2,"0");
      String min = "${updatedDateTime.minute}".padLeft(2,"0");
      String sec = "${updatedDateTime.second}".padLeft(2,"0");
      return "$month-$day-$year $hour:$min:$sec";

    }catch(e){
      return "";
    }

  }

  static void showBottomSheet({required Widget content, bool expand = false, double radius = 16}) {
    showModalBottomSheet<void>(
      context: sl<NavigationService>().navigatorKey.currentContext!,
      isScrollControlled: expand,
      builder: (BuildContext context) {
        //final theme = Theme.of(context);
        return Material(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(24.w), topRight: Radius.circular(24.w)),
          color: Theme.of(context).brightness==Brightness.dark?AppColors.darkCardBackground: AppColors.cardColorLight,
          child: content,
        );
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(24.w), topRight: Radius.circular(24.w)),
      ),
    );
  }

}