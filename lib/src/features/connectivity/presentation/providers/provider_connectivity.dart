import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/features/connectivity/data/enums/e_connection_state.dart';

class ProviderConnectivity extends ChangeNotifier{
  //states
  EConnectionState _connectionState = EConnectionState.online;

  //getters
  EConnectionState get connectionState => _connectionState;

  //setters
  set connectionState(EConnectionState state){
    _connectionState = state;
    notifyListeners();
  }
}