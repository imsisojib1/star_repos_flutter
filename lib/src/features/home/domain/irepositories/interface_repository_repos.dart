import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';

abstract class IRepositoryRepos{
  Future<ResponseStarRepos?> fetchRepos(RequestParamsRepos requestPrams);
  Future<RepoDetails?> fetchRepoDetails({required String referencePath,});
  Future<void> saveLastSortingMethod(String sortingMethod);
  String? fetchLastSortingMethod();
}