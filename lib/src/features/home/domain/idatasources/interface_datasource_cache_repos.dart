import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';

abstract class IDataSourceCacheRepos{
  Future<ResponseStarRepos?> fetchRepos(RequestParamsRepos requestPrams);
  void savePaginatedRepos(ResponseStarRepos response, RequestParamsRepos requestPrams);
  RepoDetails? fetchRepoDetails({required String referencePath,});
  void saveRepoDetails({required String referencePath,required RepoDetails details});
  void saveLastTimeFetchedFromRemoteInMilliseconds(int millis);
  int? fetchLastTimeFetchedFromRemoteInMilliseconds();
  void saveCachedReposKeys(String key);
  List<String> fetchCachedReposKeys();
  void saveCachedRepoDetailsKeys(String key);
  List<String> fetchCachedRepoDetailsKeys();
  Future<void> saveLastSortingMethod(String methodName);
  String? fetchLastSortingMethod();
  Future<void> clearCacheForFetchedRepos(String type);
}