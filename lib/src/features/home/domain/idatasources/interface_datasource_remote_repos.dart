import 'package:flutter_boilerplate_code/src/core/data/models/api_response.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';

abstract class IDataSourceRemoteRepos{
  Future<ApiResponse<ResponseStarRepos?>> fetchRepos(RequestParamsRepos requestPrams);
  Future<ApiResponse<RepoDetails?>> fetchRepoDetails({required String referencePath,});
}