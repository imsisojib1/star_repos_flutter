import 'package:flutter_boilerplate_code/src/features/home/data/models/repo.dart';

class ResponseStarRepos {
  int? totalCount;
  bool? incompleteResults;
  List<Repo>? items;

  ResponseStarRepos({this.totalCount, this.incompleteResults, this.items});

  ResponseStarRepos.fromJson(Map<String, dynamic> json) {
    totalCount = json['total_count'];
    incompleteResults = json['incomplete_results'];
    if (json['items'] != null) {
      items = <Repo>[];
      json['items'].forEach((v) {
        items!.add(Repo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['total_count'] = totalCount;
    data['incomplete_results'] = incompleteResults;
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

