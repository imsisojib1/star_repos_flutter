import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_cache_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_remote_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/irepositories/interface_repository_repos.dart';

class RepositoryRepos implements IRepositoryRepos{
  final IDataSourceCacheRepos cacheRepos;
  final IDataSourceRemoteRepos remoteRepos;

  RepositoryRepos({required this.cacheRepos, required this.remoteRepos,});

  @override
  Future<RepoDetails?> fetchRepoDetails({required String referencePath}) async {
    //first check in cache
    var data = cacheRepos.fetchRepoDetails(referencePath: referencePath);
    if(data!=null) {
      return data;
    } else{
      var apiResponse = await remoteRepos.fetchRepoDetails(referencePath: referencePath);
      //save into cache
      if(apiResponse.result!=null){
        cacheRepos.saveRepoDetails(referencePath: referencePath, details: apiResponse.result!);
      }
      return apiResponse.result;
    }
  }

  @override
  Future<ResponseStarRepos?> fetchRepos(RequestParamsRepos requestPrams) async {
    //detect if it is first time fetching by page number
    bool ableToFetchFromRemote = shouldFetchFromRemoteByLastTimeFetchingLog(cacheRepos.fetchLastTimeFetchedFromRemoteInMilliseconds());

    if(requestPrams.page==1 && (ableToFetchFromRemote || requestPrams.forceUpdate)){
      //check if able to fetch from remote, then clear cache
      var response = await remoteRepos.fetchRepos(requestPrams);
      if(response.statusCode==200){
        //return data, save to cache, clear previous cached using type
        await cacheRepos.clearCacheForFetchedRepos(requestPrams.sort);

        //save time snapshot
        cacheRepos.saveLastTimeFetchedFromRemoteInMilliseconds(DateTime.now().millisecondsSinceEpoch);
        if(response.result!=null){
          cacheRepos.savePaginatedRepos(response.result!,requestPrams);
        }
        return response.result;
      }
    }

    ResponseStarRepos? responseStarRepos = await cacheRepos.fetchRepos(requestPrams);
    if(responseStarRepos==null){
      //so call remote repository now
      var apiResponse = await remoteRepos.fetchRepos(requestPrams);
      if(apiResponse.result!=null){
        cacheRepos.savePaginatedRepos(apiResponse.result!, requestPrams);
      }
      return apiResponse.result;
    }

    //data found from cache
    return responseStarRepos;
  }

  bool shouldFetchFromRemoteByLastTimeFetchingLog(int? millis){
    if(millis==null) return true;
    if(DateTime.now().difference(DateTime.fromMillisecondsSinceEpoch(millis)).inMinutes>=30) return true;
    return false;
  }

  @override
  String? fetchLastSortingMethod() {
    return cacheRepos.fetchLastSortingMethod();
  }

  @override
  Future<void> saveLastSortingMethod(String sortingMethod) async {
    await cacheRepos.saveLastSortingMethod(sortingMethod);
  }

}