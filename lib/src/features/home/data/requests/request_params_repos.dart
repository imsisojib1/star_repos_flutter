class RequestParamsRepos {
  String query;
  int perPage;
  int page;
  String sort;
  bool forceUpdate;

  RequestParamsRepos({
    required this.query,
    required this.perPage,
    required this.page,
    required this.sort,
    this.forceUpdate=false,
  });

  @override
  String toString() {
    return 'RequestParamsRepos{query: $query, perPage: $perPage, page: $page, sort: $sort}';
  }
}
