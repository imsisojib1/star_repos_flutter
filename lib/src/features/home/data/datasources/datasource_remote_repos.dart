import 'dart:convert';

import 'package:flutter_boilerplate_code/src/config/config_api.dart';
import 'package:flutter_boilerplate_code/src/core/application/token_service.dart';
import 'package:flutter_boilerplate_code/src/core/data/models/api_response.dart';
import 'package:flutter_boilerplate_code/src/core/domain/interfaces/interface_api_interceptor.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_remote_repos.dart';
import 'package:flutter_boilerplate_code/src/helpers/debugger_helper.dart';
import 'package:http/http.dart' as http;

class DataSourceRemoteRepos implements IDataSourceRemoteRepos {
  final IApiInterceptor apiInterceptor;
  final TokenService tokenService;

  DataSourceRemoteRepos({
    required this.apiInterceptor,
    required this.tokenService,
  });

  @override
  Future<ApiResponse<RepoDetails?>> fetchRepoDetails({required String referencePath}) async {
    Debugger.info(
      title: 'DataSourceRemoteRepos.fetchRepoDetails(): request',
      data: referencePath,
    );
    http.Response response = await apiInterceptor.get(
      endPoint: "${ConfigApi.repositories}/$referencePath",
      headers: tokenService.getUnAuthHeadersForJson(),
    );

    Debugger.info(
      title: 'DataSourceRemoteRepos.fetchRepoDetails(): response',
      data: response.body,
      statusCode: response.statusCode,
    );

    try {
      if (response.statusCode == 200) {
        return ApiResponse(
            statusCode: response.statusCode,
            result: RepoDetails.fromJson(jsonDecode(response.body)));
      }
    } catch (e) {
      Debugger.info(
        title: 'DataSourceRemoteRepos.fetchRepoDetails(): parsing-error',
        data: e,
      );
    }

    return ApiResponse(
      statusCode: response.statusCode,
    );
  }

  @override
  Future<ApiResponse<ResponseStarRepos?>> fetchRepos(RequestParamsRepos requestPrams) async {
    Debugger.info(
      title: 'DataSourceRemoteRepos.fetchRepos(): request',
      data: requestPrams.toString(),
    );
    http.Response response = await apiInterceptor.get(
      endPoint: "${ConfigApi.searchRepositories}?q=${requestPrams.query}&per_page=${requestPrams.perPage}&page=${requestPrams.page}&sort=${requestPrams.sort}",
      headers: tokenService.getUnAuthHeadersForJson(),
    );

    Debugger.info(
      title: 'DataSourceRemoteRepos.fetchRepos(): response',
      data: response.body,
      statusCode: response.statusCode,
    );

    try {
      if (response.statusCode == 200) {
        return ApiResponse(
            statusCode: response.statusCode,
            result: ResponseStarRepos.fromJson(jsonDecode(response.body)));
      }
    } catch (e) {
      Debugger.info(
        title: 'DataSourceRemoteRepos.fetchRepos(): parsing-error',
        data: e,
      );
    }

    return ApiResponse(
      statusCode: response.statusCode,
    );
  }
}
