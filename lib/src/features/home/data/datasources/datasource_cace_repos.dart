import 'dart:convert';

import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_cache_repos.dart';
import 'package:flutter_boilerplate_code/src/helpers/debugger_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataSourceCacheRepos implements IDataSourceCacheRepos{
  final SharedPreferences sharedPreferences;

  DataSourceCacheRepos({required this.sharedPreferences});

  @override
  List<String> fetchCachedReposKeys() {
    return sharedPreferences.getStringList("repo_cached_keys")??[];
  }

  @override
  Future<void> saveCachedReposKeys(String key) async {
    List<String> keys = fetchCachedReposKeys();
    keys.add(key);
    await sharedPreferences.setStringList("repo_cached_keys", keys);
  }

  @override
  List<String> fetchCachedRepoDetailsKeys() {
    return sharedPreferences.getStringList("repo_details_cached_keys")??[];
  }

  @override
  Future<void> saveCachedRepoDetailsKeys(String key) async {
    List<String> keys = fetchCachedRepoDetailsKeys();
    keys.add(key);
    await sharedPreferences.setStringList("repo_details_cached_keys", keys);
  }

  @override
  int? fetchLastTimeFetchedFromRemoteInMilliseconds() {
    return sharedPreferences.getInt("last_time_repos_fetched");
  }

  @override
  RepoDetails? fetchRepoDetails({required String referencePath}) {
    String? encodedData = sharedPreferences.getString(referencePath);
    if(encodedData==null) return null;
    try{
      Debugger.debug(
        title: "DataSourceCacheRepos.fetchRepoDetails(): from-cache",
        data: encodedData,
      );
      return RepoDetails.fromJson(jsonDecode(encodedData));
    }catch(e){
      Debugger.debug(
        title: "DataSourceCacheRepos.parsing-error",
        data: e,
      );
    }
    return null;
  }

  @override
  void saveLastTimeFetchedFromRemoteInMilliseconds(int millis) async{
    await sharedPreferences.setInt("last_time_repos_fetched", millis);
  }

  @override
  Future<void> savePaginatedRepos(ResponseStarRepos response, RequestParamsRepos request) async {
    await sharedPreferences.setString("page-${request.page}-sort-${request.sort}", jsonEncode(response.toJson()));
    await saveCachedReposKeys("page-${request.page}-sort-${request.sort}");
    Debugger.debug(
      title: "DataSourceCacheRepos.savePaginatedRepos(): saved-cache",
      data: "page-${request.page}-sort-${request.sort}",
    );
  }

  @override
  Future<void> saveRepoDetails({required String referencePath, required RepoDetails details}) async {
    await sharedPreferences.setString(referencePath, jsonEncode(details.toJson()));
    await saveCachedRepoDetailsKeys(referencePath);
    Debugger.debug(
      title: "DataSourceCacheRepos.saveRepoDetails(): saved-cache",
      data: details.toJson(),
    );
  }

  @override
  Future<ResponseStarRepos?> fetchRepos(RequestParamsRepos request) async{
    await Future.delayed(const Duration(seconds: 1));
    var encodedData = sharedPreferences.getString("page-${request.page}-sort-${request.sort}");
    if(encodedData==null) return null;
    try{
      Debugger.debug(
        title: "DataSourceCacheRepos.fetchRepos(): from-cache",
        data: "page-${request.page}-sort-${request.sort}",
      );
      return ResponseStarRepos.fromJson(jsonDecode(encodedData));
    }catch(e){
      //do something
    }
    return null;
  }

  @override
  String? fetchLastSortingMethod() {
    return sharedPreferences.getString("last_sorting_method");
  }

  @override
  Future<void> saveLastSortingMethod(String methodName) async {
    await sharedPreferences.setString("last_sorting_method", methodName);
  }

  @override
  Future<void> clearCacheForFetchedRepos(String type) async {
    Debugger.debug(
      title: "DataSourceCacheRepos.clearCacheForFetchedRepos()",
    );
    List<String> cachedKeys = fetchCachedReposKeys();
    List<String> notMatchedKeys = [];
    for(var key in cachedKeys){
      if(key.contains(type)){
        await sharedPreferences.remove(key);
      }else{
        notMatchedKeys.add(key);
      }
    }

    //then clear the keys
    await sharedPreferences.setStringList("repo_cached_keys",notMatchedKeys);
  }

}