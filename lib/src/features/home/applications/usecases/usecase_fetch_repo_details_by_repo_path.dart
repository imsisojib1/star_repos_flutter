import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_boilerplate_code/src/core/data/models/failure.dart';
import 'package:flutter_boilerplate_code/src/core/domain/interfaces/interface_use_case.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/irepositories/interface_repository_repos.dart';

class UseCaseFetchRepoDetailsByRepoPath implements IUseCase<String, RepoDetails> {
  final IRepositoryRepos repositoryRepos;

  UseCaseFetchRepoDetailsByRepoPath({required this.repositoryRepos});

  @override
  Future<Either<Failure, RepoDetails>> execute(String repoPath) async {
    var response = await repositoryRepos.fetchRepoDetails(referencePath: repoPath,);
    if (response != null) {
      return Right(response);
    }
    return Left(Failure());
  }
}
