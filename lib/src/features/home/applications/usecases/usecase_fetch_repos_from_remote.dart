import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:flutter_boilerplate_code/src/core/data/models/failure.dart';
import 'package:flutter_boilerplate_code/src/core/domain/interfaces/interface_use_case.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/responses/response_star_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/idatasources/interface_datasource_remote_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/domain/irepositories/interface_repository_repos.dart';

class UseCaseFetchReposFromRemote implements IUseCase<RequestParamsRepos, ResponseStarRepos> {
  final IDataSourceRemoteRepos remoteDataSource;

  UseCaseFetchReposFromRemote({required this.remoteDataSource});

  @override
  Future<Either<Failure, ResponseStarRepos>> execute(RequestParamsRepos requestParamsRepos) async {
    var response = await remoteDataSource.fetchRepos(requestParamsRepos);
    if (response.result != null) {
      return Right(response.result!);
    }
    return Left(Failure());
  }
}
