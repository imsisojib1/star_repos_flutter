import 'package:flutter/cupertino.dart';
import 'package:flutter_boilerplate_code/di_container.dart';
import 'package:flutter_boilerplate_code/src/core/data/enums/e_loading.dart';
import 'package:flutter_boilerplate_code/src/core/data/models/empty.dart';
import 'package:flutter_boilerplate_code/src/features/home/applications/usecases/usecase_fetch_last_sorting_method.dart';
import 'package:flutter_boilerplate_code/src/features/home/applications/usecases/usecase_fetch_repo_details_by_repo_path.dart';
import 'package:flutter_boilerplate_code/src/features/home/applications/usecases/usecase_fetch_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/applications/usecases/usecase_save_last_sorting_method.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/enums/e_sort.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/requests/request_params_repos.dart';

class ProviderRepos extends ChangeNotifier {

  //states
  ELoading? _loading;
  List<Repo> _repoServeList = [];

  //for this.local usages only: QueryParams
  int _perPage = 10;
  int _currentPage = 1;
  int _totalResults = 0;
  ESort _sort = ESort.stars;

  //getters
  ELoading? get loading => _loading;

  int get totalResults => _totalResults;

  ESort get sort => _sort;

  List<Repo> get repoServeList => _repoServeList;

  //setters
  set loading(ELoading? state) {
    _loading = state;
    notifyListeners();
  }

  //on user manually changing sorting
  void updateSort(ESort eSort) {
    _sort = eSort;
    notifyListeners();
    fetchFlutterStarRepositories(forceUpdate: true,);
    saveLastSortingMethod(_sort.name);
  }

  void saveLastSortingMethod(String method) async {
    await UseCaseSaveLastSortingMethod(repositoryRepos: sl()).execute(method);
  }

  Future<void> fetchLastSortingMethod() async {
    var result = await UseCaseFetchLastSortingMethod(repositoryRepos: sl()).execute(Empty());
    result.fold(
      (error) {
        //set default sorting
        _sort = ESort.stars;
        notifyListeners();
      },
      (response) {
        if(response=="stars"){
          _sort = ESort.stars;
        }else{
          _sort = ESort.updated;
        }
        notifyListeners();
      },
    );
  }

  Future<void> fetchFlutterStarRepositories({bool forceUpdate = false,}) async {
    loading = ELoading.loading;
    _currentPage = 1;
    RequestParamsRepos requestParamsRepos = RequestParamsRepos(
      query: "Flutter",
      perPage: _perPage,
      page: _currentPage,
      sort: _sort.name,
      forceUpdate: forceUpdate, //if user change sorting while online
    );
    var result = await UseCaseFetchRepos(repositoryRepos: sl()).execute(requestParamsRepos);
    result.fold(
      (error) {
        //do something
      },
      (response) {
        _repoServeList = response.items ?? [];
        _totalResults = response.totalCount ?? 1000; //should not null!
      },
    );

    loading = null;
  }

  Future<void> fetchMoreFlutterStarRepositories() async {
    if (loading == ELoading.loadingMoreData || (_totalResults ~/ _perPage) < _currentPage) return;

    loading = ELoading.loadingMoreData;
    _currentPage += 1;
    RequestParamsRepos requestParamsRepos = RequestParamsRepos(
      query: "Flutter",
      perPage: _perPage,
      page: _currentPage,
      sort: _sort.name,
    );
    var result = await UseCaseFetchRepos(repositoryRepos: sl()).execute(requestParamsRepos);
    result.fold(
      (error) {
        //do something
      },
      (response) {
        _repoServeList.addAll(response.items ?? []);
      },
    );

    loading = null;
  }

  Future<RepoDetails?> fetchRepoDetailsByReferencePath(String? repoPath) async {
    if (repoPath == null) return null;
    RepoDetails? repoDetails;
    var result = await UseCaseFetchRepoDetailsByRepoPath(repositoryRepos: sl()).execute(repoPath);
    result.fold(
      (error) {
        //do something
      },
      (response) {
        repoDetails = response;
      },
    );
    return repoDetails;
  }
}
