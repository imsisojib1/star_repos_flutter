import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/core/presentation/widgets/image/circular_image.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo.dart';
import 'package:flutter_boilerplate_code/src/helpers/widget_helper.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class RepoTile extends StatelessWidget {
  final Repo repo;
  final Function? onTap;

  const RepoTile({super.key, this.onTap, required this.repo});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    int showMaxTopics = (repo.topics?.isEmpty ?? true)
        ? 0
        : repo.topics!.length >= 5
            ? 5
            : repo.topics!.length;

    return InkWell(
      onTap: () {
        onTap?.call();
      },
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 16.w,
          vertical: 8.h,
        ),
        padding: EdgeInsets.all(16.w),
        decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.borderColor(),
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                CircularImage(
                  imageUrl: repo.owner?.avatarUrl ?? "",
                  size: 30.h,
                  placeholder: const FlutterLogo(),
                  errorWidget: const FlutterLogo(),
                ),
                SizedBox(
                  width: 8.w,
                ),
                Expanded(
                  child: Text.rich(
                    TextSpan(
                      children: [
                        TextSpan(
                            text: repo.owner?.login,
                            style: theme.textTheme.bodyMedium?.copyWith(
                              color: AppColors.blueText,
                            )),
                        TextSpan(
                            text: "/",
                            style: theme.textTheme.bodyMedium?.copyWith(
                              color: AppColors.blueText,
                            )),
                        TextSpan(
                            text: repo.name,
                            style: theme.textTheme.bodyMedium?.copyWith(
                              color: AppColors.blueText,
                              fontWeight: FontWeight.w600,
                            )),
                      ],
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 8.h,
            ),
            repo.description == null
                ? const SizedBox()
                : Text(
                    repo.description ?? "",
                    style: theme.textTheme.bodySmall,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
            SizedBox(
              height: 8.h,
            ),
            Wrap(
              spacing: 8.w,
              runSpacing: 8.h,
              children: [
                for (int i = 0; i < showMaxTopics; i++)
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      color: AppColors.blueText.withOpacity(.1),
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: 12.w,
                      vertical: 4.h,
                    ),
                    child: Text(
                      repo.topics?[i] ?? "",
                      style: theme.textTheme.labelSmall?.copyWith(
                        color: AppColors.blueText,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  )
              ],
            ),
            SizedBox(
              height: 10.h,
            ),
            Row(
              children: [
                CircleAvatar(
                  radius: 4.w,
                  backgroundColor: AppColors.primaryColorLight,
                ),
                SizedBox(
                  width: 6.w,
                ),
                Text(
                  repo.language ?? "",
                  style: theme.textTheme.labelSmall?.copyWith(
                    color: AppColors.textSecondaryColor(),
                  ),
                ),
                SizedBox(
                  width: 6.w,
                ),
                CircleAvatar(
                  radius: 1,
                  backgroundColor: AppColors.textSecondaryColor(),
                ),
                SizedBox(
                  width: 6.w,
                ),
                Icon(
                  Icons.star_border_outlined,
                  size: 16.h,
                  color: AppColors.textSecondaryColor(),
                ),
                SizedBox(
                  width: 6.w,
                ),
                Text(
                  WidgetHelper.decorateNumberCount(repo.stargazersCount),
                  style: theme.textTheme.labelSmall?.copyWith(
                    color: AppColors.textSecondaryColor(),
                  ),
                ),
                SizedBox(
                  width: 6.w,
                ),
                CircleAvatar(
                  radius: 1,
                  backgroundColor: AppColors.textSecondaryColor(),
                ),
                SizedBox(
                  width: 6.w,
                ),
                Text(
                  WidgetHelper.decorateDateTimeAsPostingTime(repo.updatedAt),
                  style: theme.textTheme.labelSmall?.copyWith(
                    color: AppColors.textSecondaryColor(),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
