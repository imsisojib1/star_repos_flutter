import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/core/presentation/widgets/stroke_button.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/enums/e_sort.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/providers/provider_repos.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class BottomSheetSortingMethods extends StatefulWidget {
  const BottomSheetSortingMethods({
    Key? key,
  }) : super(key: key);

  @override
  State<BottomSheetSortingMethods> createState() => _BottomSheetSortingMethodsState();
}

class _BottomSheetSortingMethodsState extends State<BottomSheetSortingMethods> {
  bool sortByStars = true;

  @override
  void initState() {
    sortByStars = context.read<ProviderRepos>().sort==ESort.stars?true: false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 12.h,
              ),
              Center(
                child: Container(
                  height: 5.h,
                  width: 40.w,
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: AppColors.grey400,
                  ),
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16.w),
                child: SizedBox(
                  child: Text(
                    "Sort By",
                    style: theme.textTheme.titleMedium,
                  ),
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              Row(
                children: [
                  Checkbox(
                    value: sortByStars,
                    onChanged: (value) {
                      setState(() {
                        sortByStars = !sortByStars;
                      });
                    },
                  ),
                  Text("stars"),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                    value: !sortByStars,
                    onChanged: (value) {
                      setState(() {
                        sortByStars = !sortByStars;
                      });
                    },
                  ),
                  Text("updated"),
                ],
              ),
              SizedBox(
                height: 16.h,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(vertical: 24.h, horizontal: 24.w),
          child: StrokeButton(
            buttonText: "Apply",
            borderRadius: 8,
            onPressed: () {
              context.read<ProviderRepos>().updateSort(sortByStars?ESort.stars:ESort.updated);
              Navigator.pop(context);
            },
          ),
        ),
      ],
    );
  }
}
