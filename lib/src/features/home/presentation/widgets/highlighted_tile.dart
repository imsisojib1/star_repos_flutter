import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class HighlightedTile extends StatelessWidget {
  final String? valueText;
  final String otherText;
  final IconData iconData;

  const HighlightedTile(
      {super.key, this.valueText, required this.otherText, required this.iconData});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          iconData,
          color: AppColors.textSecondaryColor(),
          size: 16.h,
        ),
        SizedBox(
          width: 6.w,
        ),
        Text.rich(
          TextSpan(children: [
            TextSpan(
              text: valueText,
              style: theme.textTheme.labelMedium?.copyWith(
                fontWeight: FontWeight.bold,
              ),
            ),
            TextSpan(
              text: otherText,
              style: theme.textTheme.labelMedium?.copyWith(
                color: AppColors.textSecondaryColor(),
              ),
            ),
          ]),
        ),
      ],
    );
  }
}
