import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';

class RepoShimmerTile extends StatelessWidget {
  const RepoShimmerTile({Key? key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Shimmer.fromColors(
        baseColor: theme.brightness == Brightness.light
            ? AppColors.grey500
            : AppColors.grey900,
        highlightColor: theme.brightness == Brightness.light
            ? AppColors.grey400
            : AppColors.grey800,
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 16.w,
            vertical: 8.h,
          ),
          padding: EdgeInsets.all(16.w),
          decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.borderColor(),
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.white,
                  ),
                  SizedBox(
                    width: 8.w,
                  ),
                  Row(
                    children: [
                      Container(
                        height: 16.h,
                        width: 80.w,
                        color: AppColors.white,
                      ),
                      SizedBox(width: 4.w,),
                      Container(
                        height: 16.h,
                        width: 80.w,
                        color: AppColors.white,
                      ),
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 8.h,
              ),
              Container(
                height: 16.h,
                width: MediaQuery.of(context).size.width,
                color: AppColors.white,
              ),
              SizedBox(
                height: 4.h,
              ),
              Container(
                height: 16.h,
                width: MediaQuery.of(context).size.width,
                color: AppColors.white,
              ),
              SizedBox(
                height: 4.h,
              ),
              Container(
                height: 16.h,
                width: MediaQuery.of(context).size.width/2,
                color: AppColors.white,
              ),
              SizedBox(
                height: 8.h,
              ),
              Wrap(
                spacing: 8.w,
                runSpacing: 8.h,
                children: [
                  for (int i = 0; i < 5; i++)
                    Container(
                      height: 12,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: AppColors.blueText.withOpacity(.1),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: 12.w,
                        vertical: 4.h,
                      ),
                    )
                ],
              ),
              SizedBox(
                height: 10.h,
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 4.w,
                    backgroundColor: AppColors.primaryColorLight,
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  Container(
                    height: 16.h,
                    width: 50,
                    color: AppColors.white,
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  CircleAvatar(
                    radius: 1,
                    backgroundColor: AppColors.textSecondaryColor(),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  Icon(
                    Icons.star_border_outlined,
                    size: 16.h,
                    color: AppColors.textSecondaryColor(),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  Container(
                    height: 16.h,
                    width: 50,
                    color: AppColors.white,
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  CircleAvatar(
                    radius: 1,
                    backgroundColor: AppColors.textSecondaryColor(),
                  ),
                  SizedBox(
                    width: 6.w,
                  ),
                  Container(
                    height: 16.h,
                    width: 80,
                    color: AppColors.white,
                  ),
                ],
              ),
            ],
          ),
        ),);
  }
}
