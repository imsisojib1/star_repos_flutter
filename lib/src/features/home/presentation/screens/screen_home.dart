import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/core/data/enums/e_loading.dart';
import 'package:flutter_boilerplate_code/src/core/presentation/widgets/stroke_button.dart';
import 'package:flutter_boilerplate_code/src/features/connectivity/data/enums/e_connection_state.dart';
import 'package:flutter_boilerplate_code/src/features/connectivity/presentation/providers/provider_connectivity.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/providers/provider_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/widgets/bottomsheet_sorting_methods.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/widgets/repo_shimmer_tile.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/widgets/repo_tile.dart';
import 'package:flutter_boilerplate_code/src/helpers/widget_helper.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_boilerplate_code/src/resources/app_images.dart';
import 'package:flutter_boilerplate_code/src/routes/routes.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class ScreenHome extends StatefulWidget {
  const ScreenHome({super.key});

  @override
  State<ScreenHome> createState() => _ScreenHomeState();
}

class _ScreenHomeState extends State<ScreenHome> {
  ScrollController? scrollController;

  @override
  void initState() {
    scrollController = ScrollController();
    scrollController?.addListener(() {
      if (scrollController!.position.maxScrollExtent <= scrollController!.offset + (380)) {
        //load when there is reaming 2-3 tiles
        context.read<ProviderRepos>().fetchMoreFlutterStarRepositories();
      }
    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      //fist time calling, so fetching last sorting method first then calling for repos
      context.read<ProviderRepos>().fetchLastSortingMethod().then((value) {
        context.read<ProviderRepos>().fetchFlutterStarRepositories();
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      body: SafeArea(
        child: Consumer2<ProviderRepos, ProviderConnectivity>(
          builder: (_, providerRepos, providerConnectivity, child) {
            return CustomScrollView(
              controller: scrollController,
              physics: const BouncingScrollPhysics(),
              slivers: [
                SliverAppBar(
                  pinned: false,
                  toolbarHeight: kToolbarHeight + 24.h,
                  title: Text(
                    "Star Repos - \"Flutter\"",
                    style: theme.textTheme.displaySmall?.copyWith(
                      color: AppColors.primaryColorLight,
                    ),
                  ),
                  centerTitle: false,
                  automaticallyImplyLeading: false,
                  bottom: PreferredSize(
                    preferredSize: Size(MediaQuery.of(context).size.width, 24.h),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 16.w,
                            vertical: 8.h,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  color: theme.brightness == Brightness.dark
                                      ? AppColors.darkCardBackground
                                      : AppColors.cardColorLight,
                                  border: Border.all(
                                    color: AppColors.borderColor(),
                                  ),
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                padding: EdgeInsets.symmetric(
                                  horizontal: 10.w,
                                  vertical: 4.h,
                                ),
                                child: Text(
                                  "Repositories: ${providerRepos.repoServeList.length}/${providerRepos.totalResults}",
                                  style: theme.textTheme.labelSmall,
                                ),
                              ),
                              InkWell(
                                onTap: () {
                                  WidgetHelper.showBottomSheet(
                                      content: BottomSheetSortingMethods());
                                },
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: theme.brightness == Brightness.dark
                                        ? AppColors.darkCardBackground
                                        : AppColors.cardColorLight,
                                    border: Border.all(
                                      color: AppColors.borderColor(),
                                    ),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 10.w,
                                    vertical: 4.h,
                                  ),
                                  child: Text(
                                    "Sort by: ${providerRepos.sort.name}",
                                    style: theme.textTheme.labelSmall,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          width: MediaQuery.of(context).size.width,
                          color: AppColors.borderColor(),
                        ),
                      ],
                    ),
                  ),
                ),
                providerConnectivity.connectionState == EConnectionState.offline
                    ? SliverAppBar(
                        pinned: true,
                        toolbarHeight: 0,
                        bottom: PreferredSize(
                          preferredSize: Size(MediaQuery.of(context).size.width, 30),
                          child: Container(
                            //height: 24,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              color: theme.brightness == Brightness.dark
                                  ? AppColors.errorColorDark
                                  : AppColors.errorColorLight,
                            ),
                            padding: EdgeInsets.symmetric(
                              horizontal: 16.w,
                              vertical: 4,
                            ),
                            child: Center(
                                child: Text(
                              "You are browsing offline!",
                              style: theme.textTheme.labelSmall,
                            )),
                          ),
                        ),
                      )
                    : const SliverToBoxAdapter(),

                //for empty list
                providerRepos.loading==null && providerRepos.repoServeList.isEmpty?SliverFillRemaining(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(AppImages.stateEmpty,height: 200.h,width: 200.h,),
                      Text(
                        "No data found!",
                        style: theme.textTheme.bodySmall,
                      ),
                      SizedBox(
                        height: 32.h,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 24,
                        ),
                        child: StrokeButton(
                          buttonText: "Try Again",
                          onPressed: () {
                            context.read<ProviderRepos>().fetchFlutterStarRepositories();
                          },
                        ),
                      ),
                    ],
                  ),
                ): const SliverToBoxAdapter(),

                SliverList.builder(
                  itemCount: providerRepos.repoServeList.length,
                  itemBuilder: (_, index) {
                    return RepoTile(
                      repo: providerRepos.repoServeList[index],
                      onTap: () {
                        Navigator.pushNamed(
                          context,
                          Routes.repositoryDetailsScreen,
                          arguments: {
                            "repoPath": providerRepos.repoServeList[index].fullName,
                          },
                        );
                      },
                    );
                  },
                ),
                // SliverToBoxAdapter(
                //   child: providerRepos.loading == ELoading.loadingMoreData
                //       ? const Center(
                //           child: CircularProgressIndicator(),
                //         )
                //       : const SizedBox(),
                // ),

                (providerRepos.loading==ELoading.loading || providerRepos.loading==ELoading.loadingMoreData)?SliverList.builder(
                  itemCount: 3,
                  itemBuilder: (_, index) {
                    return const RepoShimmerTile();
                  },
                ):const SliverToBoxAdapter(),

                const SliverToBoxAdapter(
                  child: SizedBox(
                    height: 40,
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
