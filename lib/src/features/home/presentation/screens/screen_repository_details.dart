import 'package:flutter/material.dart';
import 'package:flutter_boilerplate_code/src/core/presentation/widgets/image/circular_image.dart';
import 'package:flutter_boilerplate_code/src/core/presentation/widgets/stroke_button.dart';
import 'package:flutter_boilerplate_code/src/features/home/data/models/repo_details.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/providers/provider_repos.dart';
import 'package:flutter_boilerplate_code/src/features/home/presentation/widgets/highlighted_tile.dart';
import 'package:flutter_boilerplate_code/src/helpers/widget_helper.dart';
import 'package:flutter_boilerplate_code/src/resources/app_colors.dart';
import 'package:flutter_boilerplate_code/src/resources/app_images.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class ScreenRepositoryDetails extends StatefulWidget {
  final Map<String, dynamic>? arguments;

  const ScreenRepositoryDetails({
    super.key,
    this.arguments,
  });

  @override
  State<ScreenRepositoryDetails> createState() => _ScreenRepositoryDetailsState();
}

class _ScreenRepositoryDetailsState extends State<ScreenRepositoryDetails> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Scaffold(
      body: SafeArea(
        child: FutureBuilder(
          future: context
              .read<ProviderRepos>()
              .fetchRepoDetailsByReferencePath(widget.arguments?["repoPath"]),
          builder: (_, AsyncSnapshot<RepoDetails?> response) {
            if (response.connectionState == ConnectionState.done) {
              RepoDetails? details = response.data;
              if (details == null) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(AppImages.state404,height: 200.h,width: 200.h,),
                    Text(
                      "No data found!",
                      style: theme.textTheme.bodySmall,
                    ),
                    SizedBox(
                      height: 32.h,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 24,
                      ),
                      child: StrokeButton(
                        buttonText: "Go Back",
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ],
                );
              }
              return CustomScrollView(
                physics: const BouncingScrollPhysics(),
                slivers: [
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 16.w,
                        vertical: 24.h,
                      ),
                      child: Row(
                        children: [
                          IconButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            icon: const Icon(
                              Icons.arrow_back_ios,
                            ),
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                CircularImage(
                                  imageUrl: details.owner!.avatarUrl!,
                                  size: 45.h,
                                  placeholder: const FlutterLogo(),
                                  errorWidget: const FlutterLogo(),
                                ),
                                SizedBox(
                                  width: 16.w,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      "${details.owner?.login}/",
                                      style: theme.textTheme.bodySmall,
                                    ),
                                    Text(
                                      details.name ?? "",
                                      style: theme.textTheme.bodyMedium?.copyWith(
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      height: 1,
                      width: MediaQuery.of(context).size.width,
                      color: AppColors.borderColor(),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 24.w,
                        vertical: 16.h,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            details.description ?? "",
                            style: theme.textTheme.bodySmall,
                          ),
                          SizedBox(
                            height: 8.h,
                          ),
                          HighlightedTile(
                            iconData: Icons.accessibility_new_outlined,
                            otherText: details.license?.name ?? "N/A",
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          Row(
                            children: [
                              HighlightedTile(
                                iconData: Icons.star_border_outlined,
                                valueText:
                                    WidgetHelper.decorateNumberCount(details.stargazersCount),
                                otherText: " stars",
                              ),
                              SizedBox(
                                width: 16.w,
                              ),
                              HighlightedTile(
                                iconData: Icons.account_tree_outlined,
                                valueText: WidgetHelper.decorateNumberCount(details.forksCount),
                                otherText: " fork",
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          Row(
                            children: [
                              HighlightedTile(
                                iconData: Icons.tag_outlined,
                                valueText: WidgetHelper.decorateNumberCount(details.topics?.length),
                                otherText: " tags",
                              ),
                              SizedBox(
                                width: 16.w,
                              ),
                              const HighlightedTile(
                                iconData: Icons.monitor_heart_outlined,
                                otherText: "Activity",
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 4.h,
                          ),
                          HighlightedTile(
                            iconData: Icons.remove_red_eye_outlined,
                            valueText: WidgetHelper.decorateNumberCount(details.watchers),
                            otherText: " watching",
                          ),
                          SizedBox(
                            height: 10.h,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      height: 1,
                      width: MediaQuery.of(context).size.width,
                      color: AppColors.borderColor(),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 24.w,
                        vertical: 16.h,
                      ),
                      child: Text(
                        "Updated at ${WidgetHelper.decorateUpdateTimeExactFormat(details.updatedAt)}",
                        style: theme.textTheme.bodySmall?.copyWith(
                          color: AppColors.textSecondaryColor(),
                        ),
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      height: 1,
                      width: MediaQuery.of(context).size.width,
                      color: AppColors.borderColor(),
                    ),
                  ),
                ],
              );
            }

            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
